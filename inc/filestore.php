<?php

class Filestore
{
    public $filename = '';

    function __construct($filename = '')
    {
        $this->filename = $filename;
    }

    /**
     * Returns array of lines in $this->filename
     */
    function readLines()
    {
        $contentArray = array();

        if (filesize($this->filename) > 0) {
            //$filesize = filesize($this->filename);
            $handle = fopen($this->filename, 'r');
            $contents = trim(fread($handle, filesize($this->filename)));
            $contentArray = explode("\n", $contents);
            fclose($handle);
        }

        return $contentArray;
    }

    /**
     * Writes each element in $array to a new line in $this->filename
     */
    function writeLines($array)
    {
       $handle = fopen($this->filename, 'w+');
       //  Implode the entire array into one string, 
       // with newlines in between each item.
       $string = implode("\n", $array);
       fwrite($handle, $string);
       fclose($handle);
    }

    /**
     * Reads contents of csv $this->filename, returns an array
     */
    function readCSV()
    {
       $content_array=[];

       $handle = fopen($this->filename, 'r');
       
       while(!feof($handle)) {
       
         $row = fgetcsv($handle);
         
         if(!empty($row)) 
           {
             $content_array [] = $row;
           }
       }
     
       fclose($handle);
       return $content_array;
    }

    /**
     * Writes contents of $array to csv $this->filename
     */
    function writeCSV($array)
    {
         $handle = fopen($this->filename, 'w');

         foreach ($array as $row) 
         {
           fputcsv($handle, $row);
         }

         fclose($handle);
     }
 }


















