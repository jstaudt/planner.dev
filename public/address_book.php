
<?php

include '../inc/address_data_store.php'; 

$Address_book_obj = new Address_book();
$Address_book_obj->filename='address_book.csv';
$addressBook = $Address_book_obj->readCSV();
$Address_book_obj->add_book=$addressBook;


if (isset($_GET['id'])) {
	$id = $_GET['id'];
	unset($addressBook[$id]);
	$Address_book_obj->writeCSV($addressBook);
}


if (!empty($_POST)) {
	if (empty($_POST['Name']) || empty($_POST['Address']) || empty($_POST['City']) || empty($_POST['State']) || empty($_POST['Zip'])) {    
   		$error = "Blank fields are invalid.";
   	} 

   	else {
   		if(isset($_POST['Name'])) {
			$new_entry['Name'] = ucfirst($_POST['Name']);
		} if(isset($_POST['Address'])) {
			$new_entry['Address'] = ucfirst($_POST['Address']);
		}if(isset($_POST['City'])) {
			$new_entry ['City'] = ucfirst($_POST['City']);
		}if(isset($_POST['State'])) {
			$new_entry ['State'] = ucfirst($_POST['State']);
		}if(isset($_POST['Zip'])) {
			$new_entry ['Zip'] = ucfirst($_POST['Zip']);
		}


		// $new_array = $_POST;
		$addressBook[]=$new_entry;
		$Address_book_obj->writeCSV ($addressBook);
	}
}

?>

<html>

	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<title>Address Book</title>
		<style>
				.body{ 
					background-image: url(/img/Codeup-logo.png);
				}
				.panel-body {
					background-color: tan;
					background-image: url(/img/Codeup-logo.png);
				}

				.table {
					background-image: url(/img/transparent_at_work.png);
					font-weight: bold;

				}
				glyphicon-remove

				
		</style>
	</head>

<body>
	<div class="page-header">
		<h1>Contacts: <small> family/friends/professional</small></h1>

	<? if (isset($error)): ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
	  		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<p> <?= $error ?> </p>
		</div>
	<? endif ?>

	<div class="panel panel-success">
	  <!-- Default panel contents -->
	<div class="panel-heading"></div>
		<div class="panel-body">
	   <!--  <p>These are your contacts.  Don't feed them after midnight.</p> -->
		</div>

		<table class="table table-hover">
			
			<tr>
				<th></th>
				<th>Name</th>
				<th>Address</th>
				<th>City</th>
				<th>State</th>
				<th>Zip</th>
			</tr>
				
				<? foreach ($addressBook as $key => $address): ?>
					<tr><td>
						<button type="button" class="btn btn-default btn-xs">
	    				 
	    					<a href="?id=<?= $key; ?>"><span class="glyphicon glyphicon-remove"></span></button></a></td>
					<? foreach ($address as $key => $value): ?>

						<td>
						<?=$value?>
						</td>
					<? endforeach;?>
					
					</tr>

				<?endforeach;?>

		</table>
	</div>

	<h4>Enter Contact</h4>

	<form method="POST" action="address_book.php">


			<label for="Name"></label>
			<input type="text" id="Name" name="Name" placeholder="enter name here"></input>

			<label for="Address"></label>
			<input type="text" id="Address" name="Address" placeholder="enter address here"></input>

			<label for="City"></label>
			<input type="text" id="City" name="City" placeholder="enter city here"></input>

			<label for="State"></label>
			<input type="text" id="State" name="State" placeholder="enter state here"></input>

			<label for="Zip"></label>
			<input type="text" id="Zip" name="Zip" placeholder="enter zip here"></input>

			<button type="submit">Submit</button>

		</form>

	<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
</body>
</html>	
